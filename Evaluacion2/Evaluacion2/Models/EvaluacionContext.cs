﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Evaluacion2Context : DbContext
    {
        public Evaluacion2Context() : base("DefaultConnection")
        {

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
        public System.Data.Entity.DbSet<Evaluacion2.Models.Specialty> Specialtys { get; set; }
        public System.Data.Entity.DbSet<Evaluacion2.Models.Headquarters> Headquarters { get; set; }
        public System.Data.Entity.DbSet<Evaluacion2.Models.Instructor> Instructors { get; set; }
    }
}