﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Instructor
    {
        [Key]
        public int InstructorId { get; set; }
        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Primer Nombre")]
        public string NombreInstructor { get; set; }

        [Display(Name = "Segundo Nombre")]
        public string SegundoNombre { get; set; }


        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [Display(Name = "Primer Apellido")]
        public string ApellidoInstructor { get; set; }


        [Display(Name = "Segundo Apellido")]
        public string segundoApellido { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(30, ErrorMessage = "El campo {0} puede contener máximo {1} y minimo {2} caracteres", MinimumLength = 3)]
        [RegularExpression("^[a-zA-Z0-9]{1,10}@*@(gmail.com|zoho.com|outlook.com)$", ErrorMessage = "No es valido el formato del correo")]
        [Display(Name = "Correo Electronico")]
        public string Correo { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        [Display(Name = "Fecha de nacimiento")]
        public DateTime FechaNacimiento { get; set; }

        [Display(Name = "Nombre de la especialidad")]
        public string NombreEspecialidad { get; set; }
        [Display(Name = "Nombre de la sede")]
        public string NombreSede  { get; set; }


        [Display(Name = "Nombre de la Especialidad")]
        public int EspecialidadID { get; set; }

        [Display(Name = "Nombre de la Sede")]
        public int HeadquartersID { get; set; }


        public virtual Specialty Specialty { get; set; }
        public virtual Headquarters Headquarters { get; set; }
        
    }
}