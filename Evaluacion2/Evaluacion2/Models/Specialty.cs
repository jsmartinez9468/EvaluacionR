﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Specialty
    {
        [Key]
        public int EspecialidadID { get; set; }

        [Display(Name = " Nombre Especialidad ")]
        public string nameSpecialty { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        public Boolean Estado { get; set; }

        public virtual ICollection<Instructor> Instructor { get; set; }
    }
}