﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Evaluacion2.Models
{
    public class Headquarters
    {
        [Key]
        public int HeadquartersID { get; set; }

        [Required(ErrorMessage = "El campo {0} es obligatorio")]
        [StringLength(20, ErrorMessage = "El campo {0} puede contener máximo {1} y minimo {2} caracteres", MinimumLength = 3)]
        public string NombreSede { get; set; }

        public virtual ICollection<Instructor> Instructor { get; set; }
    }
}