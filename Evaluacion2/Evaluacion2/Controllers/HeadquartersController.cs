﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Evaluacion2.Models;

namespace Evaluacion2.Controllers
{
    public class HeadquartersController : Controller
    {
        private Evaluacion2Context db = new Evaluacion2Context();

        // GET: Headquarters
        public ActionResult Index()
        {
            return View(db.Headquarters.ToList());
        }

        // GET: Headquarters/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headquarters headquarters = db.Headquarters.Find(id);
            if (headquarters == null)
            {
                return HttpNotFound();
            }
            return View(headquarters);
        }

        // GET: Headquarters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Headquarters/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HeadquartersID,NombreSede")] Headquarters headquarters)
        {
            if (ModelState.IsValid)
            {
                db.Headquarters.Add(headquarters);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(headquarters);
        }

        // GET: Headquarters/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headquarters headquarters = db.Headquarters.Find(id);
            if (headquarters == null)
            {
                return HttpNotFound();
            }
            return View(headquarters);
        }

        // POST: Headquarters/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HeadquartersID,NombreSede")] Headquarters headquarters)
        {
            if (ModelState.IsValid)
            {
                db.Entry(headquarters).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(headquarters);
        }

        // GET: Headquarters/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Headquarters headquarters = db.Headquarters.Find(id);
            if (headquarters == null)
            {
                return HttpNotFound();
            }
            return View(headquarters);
        }

        // POST: Headquarters/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Headquarters headquarters = db.Headquarters.Find(id);
            db.Headquarters.Remove(headquarters);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
